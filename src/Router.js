import { Routes, Route, Outlet } from "react-router-dom";

import Home from "./component/Home";
import SignIn from "./component/SignIn";
import SignUp from "./component/SignUp";
import Dashboard from "./component/Dashboard";

import React, { useContext, useEffect } from "react";
import About from "./component/About";
import PrivateRoute from "./component/PrivateRoute";
import DemoContext from "./component/context/DemoContext";

const Router = () => {
  const demoContext = useContext(DemoContext);
  const { getValue } = demoContext;
  useEffect(() => {
    getValue();
  }, []);

  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="signin" element={<SignIn />} />
      <Route path="signup" element={<SignUp />} />
      <Route path="about" element={<About />} />
      <Route
        element={
          <>
            <PrivateRoute />
          </>
        }
      >
        <Route path="dashboard" element={<Dashboard />} />
      </Route>
    </Routes>
  );
};

export default Router;
