import React, { useContext } from "react";
import DemoContext from "./context/DemoContext";

const Dashboard = () => {
  // Context
  const demoContext = useContext(DemoContext);
  const { user } = demoContext;
  const { name, phone, gender, email, date, city, state } = user;
  // console.log(user);
  return (
    <div>
      <section className="section about-section gray-bg" id="about">
        <div className="container">
          <div className="row align-items-center flex-row-reverse">
            <div className="col-lg-6">
              <div className="about-text go-to">
                <h3 className="dark-color">Welcome {name}</h3>
                <h6 className="theme-color lead">
                  A Lead UX &amp; UI designer based in India
                </h6>

                <div className="row about-list">
                  <div className="col-md-6">
                    <div className="media">
                      <label>Birthday</label>
                      <p>{date}</p>
                    </div>
                    <div className="media">
                      <label>Age</label>
                      <p>22 Yr</p>
                    </div>
                    <div className="media">
                      <label>Address</label>
                      <p>
                        {city},{state}
                      </p>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="media">
                      <label>E-mail</label>
                      <p>{email}</p>
                    </div>
                    <div className="media">
                      <label>Phone</label>
                      <p>{phone}</p>
                    </div>
                    <div className="media">
                      <label>Gender</label>
                      <p>{gender}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="about-avatar">
                <img
                  src={
                    gender === "male"
                      ? "https://bootdey.com/img/Content/avatar/avatar7.png"
                      : "https://img.icons8.com/color/350/000000/person-female.png"
                  }
                  title=""
                  alt=""
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Dashboard;
