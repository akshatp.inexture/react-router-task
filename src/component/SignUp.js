import { useContext, useEffect } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { Button } from "react-bootstrap";
import DemoContext from "./context/DemoContext";
import { useNavigate } from "react-router-dom";

const Signup = () => {
  // Context
  const demoContext = useContext(DemoContext);
  const { signUp, isAuth } = demoContext;
  // Function
  const onSubmit = (values) => {
    signUp(values);
  };
  // To navigate to dashboard if user is successfully logged in
  const navigate = useNavigate();
  useEffect(() => {
    if (isAuth) {
      navigate("/dashboard");
    }

    // eslint-disable-next-line
  }, [isAuth]);

  // yup Form validation
  const formvalidationschema = yup.object().shape({
    name: yup.string().required("name is required"),
    email: yup
      .string()
      .email("Please enter correct email")
      .required("Email is required"),
    gender: yup.string().required("Please select Gender"),
    password: yup
      .string()
      .required("Please Enter Password")
      .min(8, "Password is too short - should be 8 chars minimum.")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/,
        "please enter at least one uppercase letter, one lowercase letter, one number and one special character"
      ),
    confirmPassword: yup
      .string()
      .oneOf([yup.ref("password"), null], "Passwords must match"),
    phone: yup
      .number()
      .typeError("That doesn't look like a phone number")
      .positive("phone number can't start with a minus")
      .integer("phone number can't include a decimal point")
      .required("phone number is required"),
    date: yup.date().min(new Date("01-01-2000")).max(new Date()).required(),
    city: yup.string().required("please enter city"),
    state: yup.string().required("please enter State"),
  });
  return (
    <div className="container">
      <div className="row">
        <div className="offset-md-1 col-md-10">
          <h1> Sign Up Page </h1>
          <Formik
            initialValues={{
              name: "",
              phone: "",
              password: "",
              confirmPassword: "",
              gender: "",
              email: "",
              date: "",
              city: "",
              state: "",
            }}
            validationSchema={formvalidationschema}
            onSubmit={onSubmit}
          >
            <Form>
              <div className="form-group">
                <label>Name:</label>
                <Field
                  name="name"
                  type="text"
                  className="form-control"
                  placeholder="Enter your name"
                />
                <p className="text-danger">
                  <ErrorMessage name="name" />
                </p>
              </div>

              <div className="form-group">
                <label>Phone:</label>
                <Field
                  name="phone"
                  type="number"
                  className="form-control"
                  placeholder="Enter your Phone number"
                />
                <p className="text-danger">
                  <ErrorMessage name="phone" />
                </p>
              </div>

              <div className="form-group">
                <label>Password:</label>
                <Field
                  name="password"
                  type="password"
                  className="form-control"
                  placeholder="Enter your password"
                />
                <p className="text-danger">
                  <ErrorMessage name="password" />
                </p>
              </div>
              <div className="form-group ">
                <label>ConfirmPassword:</label>
                <Field
                  name="confirmPassword"
                  type="password"
                  className="form-control"
                  placeholder="Enter your password"
                />
                <p className="text-danger">
                  <ErrorMessage name="confirmPassword" />
                </p>
              </div>

              <div className="form-group">
                <label>Email:</label>
                <Field
                  name="email"
                  type="text"
                  className="form-control"
                  placeholder="Enter your Email"
                />
                <p className="text-danger">
                  <ErrorMessage name="email" />
                </p>
              </div>

              <label className="form-check-label me-2">Gender:</label>
              <div className="form-check form-check-inline">
                <Field
                  name="gender"
                  value="male"
                  type="radio"
                  className="form-check-input"
                />
                <label className="form-check-label">Male</label>
              </div>
              <div className="form-check form-check-inline">
                <Field
                  name="gender"
                  value="female"
                  type="radio"
                  className="form-check-input"
                />
                <label className="form-check-label">Female</label>
                <br />
                <p className="text-danger">
                  <ErrorMessage name="gender" />
                </p>
              </div>

              <div className="form-group">
                <label>Birth Date:</label>
                <Field
                  name="date"
                  type="date"
                  className="form-control"
                  placeholder="Enter your Birthday"
                />
                <p className="text-danger">
                  <ErrorMessage name="date" />
                </p>
              </div>
              <div className="form-group">
                <label>City:</label>
                <Field
                  name="city"
                  type="text"
                  className="form-control"
                  placeholder="Enter City"
                />
                <p className="text-danger">
                  <ErrorMessage name="city" />
                </p>
              </div>
              <div className="form-group">
                <label>State:</label>
                <Field
                  name="state"
                  type="text"
                  className="form-control"
                  placeholder="Enter State"
                />
                <p className="text-danger">
                  <ErrorMessage name="state" />
                </p>
              </div>
              {/* <br /> */}
              <Button variant="outline-primary" type="submit">
                Submit
              </Button>
            </Form>
          </Formik>
        </div>
      </div>
    </div>
  );
};

export default Signup;
