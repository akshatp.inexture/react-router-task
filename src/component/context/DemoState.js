import React, { useReducer } from "react";
import DemoContext from "./DemoContext";
import DemoReducer from "./DemoReducer";
import CryptoJS from "crypto-js";

const DemoState = (props) => {
  // creating initial state
  // two states one is user it will store current user data(single user) and one is users it will store different users data
  const initialState = {
    user: null,
    users: [],
    isAuth: false,
  };

  const [state, dispatch] = useReducer(DemoReducer, initialState);
  // functions

  // function for  signup
  const signUp = (data) => {
    console.log(data);
    let existingUser = state.users.find((value) => value.email === data.email);
    if (existingUser) {
      return window.alert("Please Enter Different Email");
    }
    dispatch({
      type: "Set_Data",
      payload: data,
    });
    // for encrypting user data
    var encryptUser = CryptoJS.AES.encrypt(
      JSON.stringify({ ...data, isAuth: true }),
      "AkshatPanchal"
    ).toString();
    // for storing value to localStorage
    localStorage.setItem("user", encryptUser);
    // console.log(state.users);

    // for encrypting values of users state
    var encryptUsers = CryptoJS.AES.encrypt(
      JSON.stringify([...state.users, data]),
      "AkshatPanchal"
    ).toString();
    // for storing all users data to localStorage
    localStorage.setItem("all_users", encryptUsers);
  };

  // sign is used for matching Email and password
  const signin = (user) => {
    let matchedUser = state.users.find(
      (item) => item.email === user.email && item.password === user.password
    );
    // if input credentials is wrong
    if (!matchedUser) {
      return window.alert("Invalid username or Password");
    }
    dispatch({
      type: "signin",
      payload: matchedUser,
    });

    var encryptcCurrentuser = CryptoJS.AES.encrypt(
      JSON.stringify({ matchedUser, isAuth: true }),
      "AkshatPanchal"
    ).toString();

    // for storing current user value to localstorage

    localStorage.setItem("user", encryptcCurrentuser);
  };

  const logout = () => {
    dispatch({
      type: "logout",
    });
  };

  const getValue = () => {
    // for getting encrypted data from local storage
    let encryptUser = localStorage.getItem("user");
    let encryptUsers = localStorage.getItem("all_users");

    let data = {};
    // checking if there is data in users
    if (!encryptUsers) {
      return;
    }
    // decrypting users
    var userEncrypt = CryptoJS.AES.decrypt(encryptUsers, "AkshatPanchal");
    var decryptedUsers = JSON.parse(userEncrypt.toString(CryptoJS.enc.Utf8));
    data.users = decryptedUsers;
    // checking if there is data in user
    if (encryptUser) {
      var usersEncrypt = CryptoJS.AES.decrypt(encryptUser, "AkshatPanchal");
      var decryptedUser = JSON.parse(usersEncrypt.toString(CryptoJS.enc.Utf8));
      data.user = decryptedUser;
    }

    dispatch({
      type: "get_data",
      payload: data,
    });
  };

  return (
    <div>
      <DemoContext.Provider
        value={{
          user: state.user,
          users: state.users,
          isAuth: state.isAuth,
          signUp,
          logout,
          signin,
          getValue,
        }}
      >
        {props.children}
      </DemoContext.Provider>
    </div>
  );
};

export default DemoState;
