const DemoReducer = (state, action) => {
  switch (action.type) {
    case "Set_Data":
      return {
        ...state,
        user: action.payload,
        users: [...state.users, action.payload],
        isAuth: true,
      };
    case "signin":
      return {
        ...state,

        user: action.payload,
        isAuth: true,
      };
    case "get_data": {
      return {
        ...state,
        user: action.payload.user || null,
        users: action.payload.users,
        isAuth: action.payload.user ? true : false,
      };
    }
    case "logout":
      localStorage.removeItem("user");
      return {
        ...state,
        user: null,
        isAuth: false,
      };
    default:
      return state;
  }
};

export default DemoReducer;
