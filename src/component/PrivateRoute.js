import React, { useContext } from "react";
import { Outlet, Navigate } from "react-router-dom";
import DemoContext from "./context/DemoContext";

const PrivateRoute = () => {
  const demoContext = useContext(DemoContext);
  const { isAuth } = demoContext;
  if (!isAuth) {
    return <Navigate to="/signin" />;
  }
  return <Outlet />;
};

export default PrivateRoute;
