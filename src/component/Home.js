import React from "react";
import { useNavigate } from "react-router-dom";

const Home = () => {
  const navigate = useNavigate();
  const signin = () => {
    navigate("/signin");
  };
  const signup = () => {
    navigate("/signup");
  };

  return (
    <div cla>
      <div class="bg-light p-5">
        <div className="container">
          <h1>Welcome To Home page</h1>
          <p>This is Demo of React Route</p>
          <p>
            <a
              class="btn btn-outline-primary"
              href="#"
              role="button"
              onClick={signin}
            >
              Sign In
            </a>
            <a
              class="btn btn-outline-primary ms-3"
              href="#"
              role="button"
              onClick={signup}
            >
              Sign Up
            </a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Home;
