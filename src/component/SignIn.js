import React, { useEffect } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as yup from "yup";
import { Button } from "react-bootstrap";
import DemoContext from "./context/DemoContext";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";

const SignIn = () => {
  // context
  const demoContext = useContext(DemoContext);
  const { isAuth, signin } = demoContext;
  // function
  const onSubmit = (value) => {
    console.log(value);
    signin(value);
  };

  const navigate = useNavigate();
  useEffect(() => {
    if (isAuth) {
      navigate("/dashboard");
    }
  }, [isAuth]);

  // yup validation
  const formvalidationschema = yup.object().shape({
    email: yup
      .string()
      .email("Please enter correct email")
      .required("Email is required"),
    password: yup
      .string()
      .required("Please Enter Password")
      .min(8, "Password is too short - should be 8 chars minimum."),
  });
  return (
    <div className="container">
      <div className="row">
        <div className="offset-md-1 col-md-10">
          <h1> Sign In </h1>
          <Formik
            initialValues={{
              email: "",
              password: "",
            }}
            validationSchema={formvalidationschema}
            onSubmit={onSubmit}
          >
            <Form>
              <div className="form-group">
                <label>Email:</label>
                <Field
                  name="email"
                  type="text"
                  className="form-control"
                  placeholder="Enter your Email"
                />
                <p className="text-danger">
                  <ErrorMessage name="email" />
                </p>
              </div>

              <div className="form-group">
                <label>Password:</label>
                <Field
                  name="password"
                  type="password"
                  className="form-control"
                  placeholder="Enter Your password"
                />
                <p className="text-danger">
                  <ErrorMessage name="password" />
                </p>
              </div>
              <br />
              <Button variant="outline-primary" type="submit">
                Submit
              </Button>
            </Form>
          </Formik>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
