import "./App.css";
import DemoState from "./component/context/DemoState";

import Header from "./component/Header";

import Router from "./Router";

function App() {
  return (
    // <DemoState>
    //   <Dashboard />
    // </DemoState>
    <DemoState>
      <Header />
      <Router />
    </DemoState>
  );
}

export default App;
